
#### Numérique et Sciences Informatiques à Mayotte

<small></small>

<div class="subtitle">Réunion de rentrée NSI
</div>
<img src="../img/intro.jpg" width="420" />



## Sommaire

 * L'évaluation en N.S.I
    - Épreuve terminale de spécialité
    - Le contrôle continu en 1<sup>ère</sup>
 * Recensement des projets & animations
 * Formations disciplinaires
   - Besoins
 * Création de groupes de travail
    - Thèmes
    - Calendrier
 * Outils pour la création de ressources



## L'évaluation en N.S.I.

* L'épreuve de spécialité
* Le contrôle continu (projet de guide d’évaluation du CC pour NSI)

<img src="../img/evals.jpg" width="240" />


## 2 Situations

* Pour les élèves ayant gardés les NSI en classe de terminale passent une épreuve terminale (de coefficient 16) pour l’obtention du baccalauréat. Les moyennes trimestrielles ne sont pas prises en compte dans le calcul de l’attribution du baccalauréat.
* Pour les élèves ayant abandonnés les NSI en classe de terminale, les moyennes des trois trimestres interviennent pour un coefficient de 8 sur les 100 du baccalauréat.


##  L'épreuve de spécialité
Ne sera pas l'objet d'une évaluation
* Histoire de l'informatique

  - Événements clés de l'histoire de l'informatique

* Structures de données

  - Graphes : structures relationnelles. Sommets, arcs, arêtes, graphes orientés ou non orientés

* Bases de données

  - Système de gestion de bases de données relationnelles


##  L'épreuve de spécialité
Ne sera pas l'objet d'une évaluation
* Architectures matérielles, systèmes d'exploitation et réseaux

  - Sécurisation des communications

* Langages et programmation

  - Notions de programme en tant que donnée. Calculabilité, décidabilité

  - Paradigmes de programmation


##  L'épreuve de spécialité
Ne sera pas l'objet d'une évaluation
* Algorithmique

  - Algorithmes sur les graphes

  - Programmation dynamique

  - Recherche textuelle

<a href="https://www.education.gouv.fr/bo/21/Hebdo30/MENE2121274N.htm"> BO n°30 du 29 juillet </a>


##  Le contrôle continu de première.

L'évaluation pourra s'appuyer sur des devoirs surveillés sur table ou sur machine, des devoirs en temps libre, des interrogations « flash », des présentations orales et des projets informatiques. Cette liste n’est pas exhaustive et d’autres formes d’évaluation peuvent être utilisées.


##  Le contrôle continu de première.

En plus des projets, l’entraînement à l’expression orale, qui est un exercice indispensable, peut aussi se faire via des exposés, des présentations de travaux ou des aspects historiques et culturels.


##  Le contrôle continu de première.

En termes de volume, un bon équilibre, par trimestre ou semestre, consisterait en 2 à 3 devoirs surveillés, 1 à 2 projets et 4 à 6 interrogations « flash ». A titre d’exemple, les coefficients de ces évaluations pourraient être 5 pour les projets, 3 pour les DS et 1 pour les interrogations « flash ».
Il est recommandé que la part des projets dans l’évaluation globale soit au moins de 25%.



## Besoins en formations disciplinaires

<!-- Les souhaits, notamment concernant le programme de T<sup>ale</sup> si besoin : -->

On vous écoute.

<img src="../img/formation.jpg" width="220" />


## Création de groupes de travail

Objectif : faciliter le développement de la NSI dans l'ensemble de l'académie.

<img src="../img/team.png" width="240" />


### Thématiques :
Proposition de thématiques
* Création de progression & de ressources (1<sup>ère</sup> et T<sup>ale</sup>)
* Veille didactique & technologique
* Création d'un espace de mutualisation
  - Site web commun
* Homogénéisation des ressources existantes
* ...


### Calendrier :
Proposition de calendrier :
* Vendredi 29 / 10 au labo de maths LPO de Sada
* Lundi 29 / 11 au labo de maths du LPO de Sada
* Mardi 25 / 01 
* Mercredi 23 / 02 au LPO Bamana
* Jeudi 29 / 03
* Vendredi 28 / 04
* Lundi 26 / 05 au LPO de Mamoudzou Nord



## Les projets & animations

Projets
* Présentation du projet porté par Lionnel, Yann et David


### Animations
* Concours général (Terminale)
* Concours de projet (Terminale)
* WEB Cup junior (Tous les élèves)
* Concours de dessin Turtle
* Concours « Castor informatique »


### Concours Général

1er concours général d'informatique.

Inscription du 12 novembre au 3 décembre.
<a href="  https://cgweb.education.gouv.fr">https://cgweb.education.gouv.fr</a>

<img src="../img/cgweb.png" width="180" />


### Concours de projets

Concours national de projets pour les élèves de terminale NSI.

Autour d'avril - mai


### Concours de dessin avec Turtle

<video controls autoplay src="./video/turtle.mp4" width="400"
type="video/mp4" >

Proposition d'un concours inter-établissement de dessin avec Turtle.
* Dépôt des codes jusqu'au 25 octobre
* Début des votes le 26 octobre
* Fin des votes pendant la fête de la science


### Concours Castor Informatique

_"Le concours Castor vise à faire découvrir aux jeunes l'informatique et les sciences du numérique."_

Plutôt adapté en SNT.

<img src="../img/castor.png" width="100" />

Donne accès au concours Algorea

<img src="../img/algorea.png" width="100" />


## Outils pour la création de ressources

* Quels outils utilisez-vous pour la création de vos ressources ?

<img src="../img/tools.png" width="280" />


### Présentation d'outils et de ressources

* Python sur le WEB
  - <a href="https://skulpt.org/"> Skulpt.js </a>  et <a href="https://codemirror.net/"> CodeMirror</a>:  <a href="https://exomorphisme.fr/cours/pnsi/th6a/parcours_turtle/exo2/" target="_blank">Exemple</a>
  - <a href="https://basthon.fr/" target="_blank"> Basthon  (Folium, PIL)</a> : <a href="https://exomorphisme.fr/basthon/graph/?from=/static/cours/tcomp/ch1/TP_traitement_image/pil-image.py&aux=/static/cours/tcomp/ch1/TP_traitement_image/loulou.pgm&module=/static/cours/tcomp/ch1/TP_traitement_image/utils.py">Exemple</a>

* Bibliothéques de rendu :
  - Katex :  Rendu latex
  - Highlight.js : Coloration syntaxique


### Présentation d'outils et de ressources

 * Création de graphe
  - <a href="gitlab.com/sebhoa/pygraph" >pyGraph</a> : <a href="https://sebhoa.gitlab.io/iremi/01_Graphes/pygraph/" target="_blank">Tuto</a>
 * Linux sur le WEB
  - <a href="https://bellard.org/jslinux/vm.html?url=alpine-x86.cfg&mem=192" target="_blank">JSlinux</a>
  - <a href="https://luffah.xyz/bidules/Terminus/">Terminus</a>

 * Site web commun


### Exemple de ressource : page web interactive

Activité SQL d'Etienne Clave, portée en ligne.

<small><a href="https://gbourel.frama.io/enquetesql">https://gbourel.frama.io/enquetesql</a></small>

<iframe width="80%" height="420" src="https://gbourel.frama.io/enquetesql">

* Quels outils utilisez-vous pour la création de vos ressources ? 

### Exemple de ressource : page web interactive

Activité de Sciences de l'Ingénieur

<small><a href="https://gbourel.frama.io/supergrave">https://gbourel.frama.io/supergrave</a></small>

<iframe width="80%" height="420" src="https://gbourel.frama.io/supergrave">



## Voilà
