from flask import Flask, jsonify, request, render_template
from ressources_utils import *
# Config de l'upload a terminer car Allowed_extension n'est pas utilisé
UPLOAD_FOLDER = './static/tmp'
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'html','zip'])
app = Flask(__name__, template_folder='template', static_folder='static')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.logger.setLevel("INFO")
db = Connecteur("../db/database.db")

@app.route('/', methods=['GET', 'POST'])
def accueil():
    """
    Gestion de la session utilisateur
    """
    return render_template('index.html')


@app.route('/ressources/')
@app.route('/ressources/<classement>')
def ressources(classement=None):
    ressources = db.get_all_ressources()
    if classement == "populaires":
        ressources.sort(key=lambda x: x["note"])
        return render_template("ressources.html",ressources = ressources)
    return render_template("ressources.html",ressources = ressources)


@app.route('/ressource/')
@app.route('/ressource/<ressource_id>')
def ressource(ressource_id=None):
    """
    Page d'affichage d'une ressource à partir de son id
    """
    if ressource_id:
        ressource = db.get_ressource_by_id(ressource_id)
        commentaires = db.get_commentaires(ressource_id)
        return render_template('ressource.html', ressource=ressource, commentaires=commentaires)
    return render_template('index.html')


@app.route('/add_ressource/', methods=['GET', 'POST'])
def add_ressource():
    if request.method == "POST":
        pass
    else:
        return render_template("add_ressource.html")


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5002, debug=True)
 
