from oauthlib.oauth2 import WebApplicationClient
from flask import Flask, jsonify, request
from auth_utils import *
import requests
import logging

FORMAT = '%(asctime)-15s %(clientip)s %(user)-8s %(message)s'
logging.basicConfig(format=FORMAT)
logger = logging.getLogger('auth')

app = Flask(__name__)


def get_token(code, redirect_uri):
    """
    Récupèration du token d'accès
    @params
        - code (str) : code  récupérer par l'authentification
        - redirect_uri (str) : url de redirection

    @returns    
        - token (str) : token d'accès aux infos utilisateur
    """
    headers = {"Content-Type":"application/x-www-form-urlencoded",
        f"Authorization" : "Basic  {pass_base64}",
        "Accept" : "application/json", 
        "charset" : "UTF-8"}
    data =  {"grant_type":"authorization_code",
        "code":code,
        "redirect_uri":redirect_uri}
    r = requests.post("https://mayotte.opendigitaleducation.com/auth/oauth2/token", headers=headers, data=data)
    data = r.json()
    try:
        return {"data":data["acces_token"], "error":None}
    except Exception as e:
        logger.warning(f'Authentification : Erreur dans la récupération du token : \n {e}')
        return {"data":"", "error":"Erreur dans la récupération du token"}


def get_userinfo(access_token):
    """
    Récupère les informations de l'utilisateur à partir du token
    @params
        - acces_token (str) : token de l'utilisateur délivrer par neo
    
    @returns
        - data (dict) : 
    """
    headers = {
        f"Authorization":f"Bearer {access_token}"
    }
    r = requests.get("https://mayotte.opendigitaleducation.com/auth/oauth2/userinfo", headers=headers)
    data = r.json()
    return data


@app.route('/login/', methods=['GET', 'POST'])
def login():
    """
    Authentification des utilisateurs
    """
    if request.method == 'POST':
        user = request.form.get("user")
        password = request.form.get("password")
        if user == "admin" and password == "m2ppsA$182u%":
            session["type"] = "admin"
            session["uid"] = "admin"
            session["classe"] = ""
            return redirect(url_for("exo_dashboard"))
        elif not user == "admin":
            return render_template(
                "auth_utilisateur.html",
                message="L'identifiant n'est pas valide")
        elif not password == "diu":
            return render_template(
                "auth_utilisateur.html",
                message="Le mot de passe n'est pas valide")
    return render_template("auth_utilisateur.html")


@app.route('/oauth/')
@app.route('/oauth/<path:final_redirect_uri>', methods = ["GET"])
def oauth(final_redirect_uri=None):
    if not final_redirect_uri:
        final_redirect_uri = "https://exomorphisme.fr/"
    client = WebApplicationClient('exomorphisme')
    request_uri = client.prepare_request_uri('https://mayotte.opendigitaleducation.com/auth/oauth2/auth',
                        redirect_uri=f"{app["BASE"]}/next/", 
                        scope=['userinfo'],
                        state="blip" )
    return redirect(request_uri)


@app.route('/next/')
def next():
    access_token = get_token(code, redirect_uri)
    user_info = get_userinfos(token)




    # Si implementation de message Flash privilégier
    # [session.pop(key) for key in list(session.keys()) if key != '_flashes']
    return render_template("/")

 

if __name__ == "__main__":
    app.run(host="127.0.0.1", port=5000)
