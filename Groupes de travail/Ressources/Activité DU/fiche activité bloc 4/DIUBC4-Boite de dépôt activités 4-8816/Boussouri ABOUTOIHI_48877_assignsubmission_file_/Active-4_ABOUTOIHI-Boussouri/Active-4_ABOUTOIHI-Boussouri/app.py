## app.py ##
### ABOUTOIHI Boussouri  ###
#### Activité de bloc - 4 ###
######
"""
CREATE USER 'bloc4'@'localhost' IDENTIFIED WITH mysql_native_password BY '***';GRANT SELECT, INSERT, UPDATE, DELETE, FILE ON *.* TO 'bloc4'@'localhost';ALTER USER 'bloc4'@'localhost' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;GRANT ALL PRIVILEGES ON `activite_bloc4`.* TO 'bloc4'@'localhost';

"""
""" Importation des librairies  """
from flask import Flask,render_template, flash, redirect , url_for , session ,request, logging
from flask_mysqldb import MySQL
from wtforms import Form, StringField , TextAreaField ,PasswordField , validators
from passlib.hash import sha256_crypt
from functools import wraps

app = Flask(__name__)
app.debug = True


#Configuration de MySQL <--> base de données
app.config['MYSQL_HOST'] = 'localhost' ## l'adresse de la machine hebergeant la base de donnée "#
app.config['MYSQL_USER'] = 'bloc4'     ## Le nom d'utilisateur ou identifiant #
app.config['MYSQL_PASSWORD'] = 'bloc4' ## Le mot de passe pour se connecter à la base de donnée ##
app.config['MYSQL_DB'] = 'activite_bloc4' ## Le nom de la base de donnée #

#initiation de la variable MYSQL au parametre de connection à la base de donnée
mysql = MySQL(app)


#livre = Livres()
# Declaration de la route '/'
@app.route('/')
def index():
    return render_template('home.html')

@app.route('/about')
def about():
    return render_template('about.html')

@app.route('/articles')
def articles():

        #create cursor
        cur = mysql.connection.cursor()

        #get articles
        result = cur.execute("SELECT * FROM articles")

        articles = cur.fetchall()

        if result > 0:
            return render_template('articles.html',articles=articles)
        else:
            msg = 'No Articles Found'
            return render_template('articles.html',msg=msg)
        #close connection
        cur.close()


"""#  Déclaration de la route '/article/<string:id>/' et le passage de l'identifiant en paramètre de type string /<string:id>/"""
@app.route('/article/<string:id>/')
def article(id):
    """#create cursor : connection à la base de donnée"""
    cur = mysql.connection.cursor()

    """#get article : Recupération de l'article de l'identité (id) """
    result = cur.execute("SELECT * FROM articles WHERE id = %s",[id])

    article = cur.fetchone()

    return render_template('article.html',article=article) ## Redirection vers la page article.html et passage de l'objet article en paramètre

""" Formulaire de création de compte utilisateur  """
class RegisterForm(Form):
    name = StringField('Name',[validators.Length(min=1,max=50)])
    username = StringField('Username',[validators.Length(min=4,max=25)])
    email = StringField('Email',[validators.Length(min=4,max=25)])
    password = PasswordField('Password', [ validators.DataRequired (),validators.EqualTo('confirm',message ='Le mot de passe n\'existe pas')])
    confirm = PasswordField('Confirm password')

@app.route('/register', methods=['GET','POST'])
def register():

    """ Recuperation du formulaire envoyer par l'utilisateur """
    form = RegisterForm(request.form)

    """ On teste si l'envoie est de type POST et récuperation des saisies """
    if request.method == 'POST' and form.validate():
        name = form.name.data
        email = form.email.data
        username = form.username.data
        password = sha256_crypt.encrypt(str(form.password.data))

        # Create crusor
        cur = mysql.connection.cursor()

        """ création d'un nouveau utlisateur   """
        cur.execute("INSERT INTO users(name,email,username,password) VALUES(%s,%s,%s,%s)",(name,email,username,password))

        """ # commit to DB = enregistrement des données à la base de données """
        mysql.connection.commit()
        """# close connection = fermeture de canal de communication """
        cur.close()

        """ Affichage du message informant l'utilisateur la création d'un nouveau compte  """
        flash(" Création de compte validé. Vous pouvez vous connecter à présent " , 'success')

        """ Redirection vers la page d'authentification  """
        redirect(url_for('login'))
    return render_template('register.html',form=form)

## Authentification des utilisateurs
@app.route('/login',methods =['GET','POST'])
def login():
    if request.method == 'POST':
        """ #Get Form Fields : Récupération des valeurs de champs saisis par l'utilisateur """
        username = request.form['username']
        password_candidate = request.form['password']

        """ # Create cursor : connection à a base de donnée """

        cur = mysql.connection.cursor()

        #Get user by username
        result = cur.execute("SELECT * FROM users WHERE username = %s" ,[username])

        if result > 0:
        # Get Stored hash
            data = cur.fetchone()
            password = data['password']

            # Compare Passwords
            if sha256_crypt.verify(password_candidate,password):
                #Passed
                session['logged_in'] = True
                session['username'] = username

                flash('Vous êtes connecté ','success')
                return redirect(url_for('dashboard'))
            else:
                error = 'Utilisateur n\'existe pas'
                return render_template('login.html',error=error)
                #close connection
            cur.close()

        else:
            error = 'Utilisateur n\'existe pas '
            return render_template('login.html',error=error)

    return render_template('login.html')

""" Verification si l'utilisateur est connecté """
def is_logged_in(f):
    @wraps(f)
    def wrap(*args,**kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            flash('Accès interdit, Identifiez vous ','danger')
            return redirect(url_for('login'))
    return wrap


""" Deconnection de l'utilisateur est connecté et destruction des variables sessions"""
@app.route('/logout')
@is_logged_in
def logout():
    session.clear()
    flash('Vous êtes déconnecté ','success')
    return redirect(url_for('login'))
# Dashboard
@app.route('/dashboard')
@is_logged_in
def dashboard():

    #create cursor
    cur = mysql.connection.cursor()

    #get articles
    result = cur.execute("SELECT * FROM articles")

    articles = cur.fetchall()

    if result > 0:
        return render_template('dashboard.html',articles=articles)
    else:
        msg = 'Pas d\'article trouvé'
        return render_template('dashboard.html',msg=msg)
    #close connection
    cur.close()

#Article form class

class ArticleForm(Form):
    title = StringField('Title',[validators.Length(min=1,max=50)])
    body = TextAreaField('Body',[validators.Length(min=30,max=1000)])

#Add Article

@app.route('/add_article', methods=['GET','POST'])
@is_logged_in
def add_article():
    form = ArticleForm(request.form)
    if request.method == 'POST' and form.validate():
        title = form.title.data
        body = form.body.data

        # Create a cursor

        cur = mysql.connection.cursor()

        #execute

        cur.execute("INSERT INTO articles(title,body,author) VALUES(%s, %s, %s)",(title, body, session['username']))

        #commit to db

        mysql.connection.commit()

        #close connection
        cur.close()

        flash('Article created ','success')

        return redirect(url_for('dashboard'))

    return render_template('add_article.html',form=form)

#Edit Article

@app.route('/edit_article/<string:id>', methods=['GET','POST'])
@is_logged_in
def edit_article(id):
    # Create cursor
    cur = mysql.connection.cursor()
    #get article by id
    result = cur.execute("SELECT * FROM articles WHERE id = %s", [id])

    article = cur.fetchone()

    #get form
    form = ArticleForm(request.form)

    #populate article form fields
    form.title.data = article['title']
    form.body. data = article['body']

    if request.method == 'POST' and form.validate():
        title = request.form['title']
        body = request.form['body']

        # Create a cursor

        cur = mysql.connection.cursor()

        #execute

        cur.execute("UPDATE articles SET title=%s, body=%s WHERE id = %s" , (title,body,id))

        #commit to db

        mysql.connection.commit()

        #close connection
        cur.close()

        flash('article mise à jour ','success')

        return redirect(url_for('dashboard'))

    return render_template('edit_article.html',form=form)

#Delete article
@app.route('/delete_article/<string:id>', methods=['POST'])
@is_logged_in
def delete_article(id):
    # Create cursor
    cur = mysql.connection.cursor()

    #Execute
    cur.execute("DELETE FROM articles WHERE id = %s",[id])

    #Commit to DB

    mysql.connection.commit()
    #close connection

    cur.close()

    flash('Article Deleted  ','success')

    return redirect(url_for('dashboard'))

#### Travail à faire  #####

""" Associer un article à une catégorie """
""" Attribuer un prix à un article """


if __name__ =='__main__':
    app.secret_key='secret123'
    app.run()
