-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 25 juin 2020 à 12:01
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `activite_bloc4`
--

-- --------------------------------------------------------

--
-- Structure de la table `articles`
--

DROP TABLE IF EXISTS `articles`;
CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `body` varchar(2000) NOT NULL,
  `author` varchar(50) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `articles`
--

INSERT INTO `articles` (`id`, `title`, `body`, `author`, `create_date`) VALUES
(1, 'Spécialité Numérique et sciences informatiques ', 'Cet ouvrage propose un cours structuré couvrant l\'intégralité du programme de la spécialité NSI (Numérique et sciences informatiques) en classe de première. Il offre 30 leçons clés en main, accompagnées d\'exercices corrigés et d\'encarts thématiques et historiques permettant d\'approfondir les leçons. Des sujets de projets collaboratifs et pluridisciplinaires viennent compléter l\'ouvrage.\r\n\r\nCe livre est constitué de 5 parties :\r\n\r\n- apprentissage de la programmation avec le langage Python\r\n\r\n- notions de base d\'algorithmique\r\n\r\n- traitement des données\r\n\r\n- architecture des ordinateurs\r\n\r\n- interaction, communication et web.\r\n\r\nLe site qui accompagne cet ouvrage fournit du matériel librement téléchargeable, comme du code source Python, des fichiers de données pour les projets ou encore un aide-mémoire Python.', 'boussouri', '2020-06-25 13:55:55'),
(2, 'Microsoft Surface Book 2', 'Informations sur le produit\r\nProcessor Description: Intel Core i7 | Taille: 15 pouces | Style: 16Go RAM, GPU 1To\r\nDescriptif technique\r\nMarque	Microsoft\r\nNuméro du modèle de l\'article	FVH-00005\r\nCouleur	Gris\r\nGarantie constructeur	1 an\r\nSystème d\'exploitation	Windows 10 Professionnel\r\nDescription du clavier	AZERTY (French keyboard)\r\nMarque du processeur	Intel\r\nType de processeur	Core i7\r\nNombre de coeurs	4\r\nTaille de la mémoire vive	16 GB\r\nTaille du disque dur	1 TB\r\nTechnologie du disque dur	Hybride (Disque Dur + SSD)\r\nType d\'écran	LCD PIXEL SENSE\r\nTaille de l\'écran	15 pouces\r\nRésolution de l\'écran	3240 x 2160\r\nRésolution maximale d\'affichage	3240 x 2160\r\nDescription de la carte graphique	Intel® CoreTM i7-8650U w/ 4.2GHz Max Turbo\r\nGPU	Intel\r\nMémoire vive de la carte graphique	16\r\nType de connectivité	Bluetooth\r\nType de technologie sans fil	Bluetooth\r\nBluetooth	Oui\r\nInterface du matériel informatique	HDMI, USB, VGA\r\nNombre de ports HDMI	1\r\nNombre de ports USB 3.0	3\r\nType de connecteur	Bluetooth WIRELESS 4.1\r\nDurée de vie moyenne (en heures)	17 heures\r\nDimensions de l\'article L x L x H	34,3 x 25,1 x 1,4 cm\r\nPoids du produit	1.91 kilogrammes', 'boussouri', '2020-06-25 13:55:55');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=CSV DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`name`, `email`, `username`, `password`) VALUES
('boussouri', 'bouss.ab@hotmail.com', 'boussouri', 'boussouri');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
