objets = [[420, 15], [300, 9], [250, 2], [210, 7], [50, 4]]
poids_maxi = 20
choisis = []
poids = 0
valeur = 0
for objet in objets:
    if objet[1] < poids_maxi:
        choisis.append(objet)
        poids_maxi -= objet[1]
        poids += objet[1]
        valeur += objet[0]
print()
print("Objets choisis :", choisis)
print("Poids total :", poids, "kg, valeur totale :", valeur, "€")
