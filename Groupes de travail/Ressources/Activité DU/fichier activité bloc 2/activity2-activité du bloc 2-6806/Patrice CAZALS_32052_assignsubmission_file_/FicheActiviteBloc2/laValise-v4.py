objets = [[420, 15], [300, 9], [250, 2], [210, 7], [50, 4]]
for objet in objets:
    objet[0] = objet[0]/objet[1]
objets.sort(reverse=True)
print(objets)
poids_maxi = 20
choisis = []
poids = 0
valeur = 0
for objet in objets:
    if objet[1] < poids_maxi:
        choisis.append(objet)
        poids_maxi -= objet[1]
        poids += objet[1]
        valeur += objet[0]*objet[1]
print("Objets choisis :", choisis)
print("Poids total :", poids, "kg, valeur totale :", valeur, "€")
