# La caisse

def rendu_monnaie(somme, valeurs):
    rendues = []
    for valeur in valeurs:
        while somme >= valeur:
            somme -= valeur
            rendues.append(valeur)
    return rendues

valeurs = [50, 20, 10, 5, 2, 1]
quitter = False
while not quitter:
    somme = int(input("Somme à rendre (en €) ? "))
    if somme == 0:
        quitter = True
    else:
        rendues = rendu_monnaie(somme, valeurs)
        print("À rendre :", rendues)
