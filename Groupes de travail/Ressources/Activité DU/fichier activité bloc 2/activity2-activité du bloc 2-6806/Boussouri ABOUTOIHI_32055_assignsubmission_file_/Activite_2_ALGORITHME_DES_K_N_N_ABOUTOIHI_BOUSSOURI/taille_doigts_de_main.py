# ABOUTOIHI Boussouri
#Exemple d'installation d'un module
# Via le terminal de commande
#C:\Users\Oumine\AppData\Local\Programs\Python\Python38-32\Scripts>pip3.8.exe install seaborn

# Sur EduPython installation d'un module il faut proceder ainsi : Outils -> Outils - >installation d'un nouveau module
# ensuite faire un Choix 2 (pip)
# puis vous tapez le nom du module.

import pandas
import matplotlib.pyplot as plt
import math
from matplotlib.widgets import TextBox
from matplotlib.widgets import Widget
from matplotlib.widgets import Button

#traitement CSV
iris=pandas.read_csv("iris.csv")
x=iris.loc[:,"majeur_length"]
y=iris.loc[:,"annulaire_width"]
lab=iris.loc[:,"suspects"]
#fin traitement CSV

# déclaration des variables
majeur = 8
annulaire = 7
k = 1

cible = [majeur,annulaire,k]
distances = list() # [ La distance |  valeur de majeur | valeur de annulaire | position dans le fichier CSV ]
saisie_majeur = False
saisie_annulaire = False
saisie_knn = False

d=list(zip(x,y))
# fin déclaration variables

#algorithme knn
def knn(cible, d):
  # retourne un tableau des distances entre la donée introduite  et le reste des données
  for i in range(len(d)) :
    dist = distance_euclidienne(cible, d[i])
    ##lab[0,i] = dist
    distances.append([dist,d[i],i])
  #pour gagner du temps ; ne pas trier ; chercher directement les k plus petit
  distances.sort(key=lambda x: x[0])
  return distances;

def distance_euclidienne(cible, d):
  somme = 0
  for i in range(len(d)):
    somme += math.pow((cible[i]-d[i]) ,2)

  return math.sqrt(somme)

#Appel de la fonction knn en paramètre la cible et les données
knn(cible, d)

#Affichage des résultats dans la console Python
print("k plus proches voisins trier ")
print('')
print(" Majeur : {}  Annulaire : {}  K : {} ".format(majeur,annulaire,k))
print('')
print("La distance |  valeur de majeur | valeur de annulaire | position dans le fichier CSV ")
print('')
for w in range(len(distances)):
    print(" {} ".format(distances[w]))

#graphique legende
plt.scatter(x[lab == 0], y[lab == 0], color='g', label='Les suspects')
plt.scatter(x[lab == 1], y[lab == 1], color='r', label='Suspect potentiel')
plt.scatter(x[lab == 2], y[lab == 2], color='k', label='Donnée saisie')
plt.scatter(majeur, annulaire, color='k')
plt.legend()

txt="Résultat : Lire la console Python pour voir les résultats "

plt.text(5,1.2, "Annulaire : {} cm Majeur : {} cm".format(annulaire,majeur), fontsize=10)
plt.text(5,0.8, "k : {}".format(k), fontsize=10)
plt.text(5,0.4, txt, fontsize=10)
#fin graphique legende


#Affichage de zone de saisie
axboxm = plt.axes([0.29,0.01,0.1,0.08])#plt.axes([Position X,Position Y,Hight du champ, width du champ])
text_box_m = TextBox(axboxm, 'Taille Majeur:')
axboxa = plt.axes([0.6,0.01,0.1,0.08])
text_box_a = TextBox(axboxa, 'Taille Annulaire:')
axboxk = plt.axes([0.77,0.01,0.08,0.08])
text_box_k = TextBox(axboxk, 'K:')
#axboxb = plt.axes([0.75,0.01,0.1,0.08])
#bouton = Button(axboxb, 'Valider')
#Fin de zone de saisie

#def clicked():
 #   print('Cliquer Boutonvalider')
  #  "cible = [text_box_m(),text_box_a(),text_box_k()]

def text_box_majeur(text):
    print("Valeur de majeur {} ".format(float(eval(text))))
    return eval(text)

def text_box_annulaire(text):
    print("Valeur de annulaire {} ".format(float(eval(text))))
    return eval(text)

def text_box_knn(text):
    print("Valeur de K {} ".format(float(eval(text))))
    return eval(text)

text_box_m.on_text_change(text_box_majeur)
text_box_a.on_text_change(text_box_annulaire)
text_box_k.on_text_change(text_box_knn)
#bouton.on_clicked(clicked)
#text_box.on_submit(submit)

# Affiche la fenettre
plt.show()
