# -*- coding: utf-8 -*-

def table_sauts(motif):
    tailleMotif = len(motif)
    table = {}
    i = 0
    for lettre in motif[:-1]:
        table[lettre] = tailleMotif - i - 1
        i = i + 1
    return table


def boyer_moore_horspool(texte, motif):
    tailleTexte = len(texte)
    tailleMotif = len(motif)
    positions = []
    if(tailleMotif<=tailleTexte):
        decalage = table_sauts(motif)
        i=0
        trouve=False
        while(i<=tailleTexte-tailleMotif):
            for j in range (tailleMotif-1,-1,-1):
                trouve=True
                if(texte[i+j]!=motif[j]):
                    if(texte[i+j]in decalage and decalage[texte[i+j]]<=j):
                        i+=decalage[texte[i+j]]
                    else:
                        i+=j+1
                    trouve=False
                    break
            if(trouve):
                positions.append(i)
                i=i+1
                trouve=False
    return positions

T="Je vais acheter de beaux poissons chez le poissonnier"
M="poisson"
print(boyer_moore_horspool(T,M))