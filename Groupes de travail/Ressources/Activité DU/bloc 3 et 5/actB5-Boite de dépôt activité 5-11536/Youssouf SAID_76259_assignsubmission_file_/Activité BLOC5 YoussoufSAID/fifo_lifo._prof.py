#!/usr/bin/env python
# -*- coding: utf-8 -*-


# Implémentation simple
# des structures FIFO et LIFO en utilisant la POO de python
# pour l'introduction de ces concepts
# exemple de stockage et prelevement FIFO - file d'attente à la caisse
# exemple de stockage et prelevement LIFO - pile d'assiètes sales


# ----------------------- FIFO ----------------------------

class FIFO:
    def __init__(self):
        # tableau interne de l'objet
        self.list = []

    # FIFO - First In First Out
    # On ajoute donc les elements en fin de tableau
    # Et on sort toujours le premier élèment donc etant donné qu'il est le plus "vieux"
    def ajouter(self, valeur):
        # On utilise append qui ajoute la valeur passé en fin de tableau
        self.list.append(valeur)

    def sortir(self):
        # pop renvoie et supprime l'élèment à l'indice spécifié
        # FIFO - on renvoie donc le premier élèment si la liste n'est pas vide
        # sinon on renvoi None
        if(len(self.list) > 0):
            return self.list.pop(0)
        else:
            return None


# Test de notre implémentation
# Ordre d'arrivée : B -> C -> A -> D
ma_fifo = FIFO()
ma_fifo.ajouter("B")
ma_fifo.ajouter("C")
ma_fifo.ajouter("A")
ma_fifo.ajouter("D")

print("FIFO: sorties:")
# Doit sortir dans l'ordre : B, C, A, D
print(ma_fifo.sortir())
print(ma_fifo.sortir())
print(ma_fifo.sortir())
print(ma_fifo.sortir())

print("----\n")
# ----------------------- LIFO ----------------------------


class LIFO:
    def __init__(self):
        # tableau interne de l'objet
        self.list = []

    # LIFO - Last In First Out
    # On ajoute donc les elements en fin de tableau
    # Et on sort toujours le dernier élèment donc etant donné qu'il est le plus "jeune"
    def ajouter(self, valeur):
        # On utilise append qui ajoute la valeur passé en fin de tableau
        self.list.append(valeur)

    def sortir(self):
        # la fonction pop renvoie et supprime l'élèment à l'indice spécifié
        # LIFO - on renvoie donc le dernier élèment de la liste
        # sinon on renvoi None
        lastValidIndex = len(self.list) - 1
        if(lastValidIndex >= 0):
            return self.list.pop(lastValidIndex)
        else:
            return None


# Test de notre implémentation
# Ordre d'arrivée : B -> C -> A -> D
ma_lifo = LIFO()
ma_lifo.ajouter("B")
ma_lifo.ajouter("C")
ma_lifo.ajouter("A")
ma_lifo.ajouter("D")
# Doit sortir dans l'ordre : D, A, C, B
print("LIFO: sorties:")
print(ma_lifo.sortir())
print(ma_lifo.sortir())
print(ma_lifo.sortir())
print(ma_lifo.sortir())
