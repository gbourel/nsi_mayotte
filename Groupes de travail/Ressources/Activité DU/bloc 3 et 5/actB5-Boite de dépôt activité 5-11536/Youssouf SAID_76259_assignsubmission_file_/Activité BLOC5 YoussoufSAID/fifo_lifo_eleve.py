#!/usr/bin/env python
# -*- coding: utf-8 -*-


# Implémentation simple
# des structures FIFO et LIFO en utilisant la POO de python
# pour l'introduction de ces concepts


# ----------------------- FIFO ----------------------------

class FIFO:
    def __init__(self):
        # tableau interne de l'objet
        self.list = []

    # FIFO - First In First Out

    def ajouter(self, valeur):
        # array.append(e) permet d'ajouter e à la fin du tableau
        # Effacer le return et implementer la fonction correctement
        return None

    def sortir(self):
        # array.pop(index) renvoie et supprime l'élèment à l'index spécifié
        # Effacer le return et implementer la fonction correctement
        return None


# Test de notre implémentation
# Ordre d'arrivée : B -> C -> A -> D
ma_fifo = FIFO()
ma_fifo.ajouter("B")
ma_fifo.ajouter("C")
ma_fifo.ajouter("A")
ma_fifo.ajouter("D")

print("FIFO: sorties:")
# Doit sortir dans l'ordre : B, C, A, D
print(ma_fifo.sortir())
print(ma_fifo.sortir())
print(ma_fifo.sortir())
print(ma_fifo.sortir())

print("----\n")
# ----------------------- LIFO ----------------------------


class LIFO:
    def __init__(self):
        # tableau interne de l'objet
        self.list = []

    # LIFO - Last In First Out

    def ajouter(self, valeur):
        # array.append(e) permet d'ajouter e à la fin du tableau
        # Effacer le return et implementer la fonction correctement
        return

    def sortir(self):
        # array.pop(index) renvoie et supprime l'élèment à l'index spécifié
        # Effacer le return et implementer la fonction correctement
        return None


# Test de notre implémentation
# Ordre d'arrivée : B -> C -> A -> D
ma_lifo = LIFO()
ma_lifo.ajouter("B")
ma_lifo.ajouter("C")
ma_lifo.ajouter("A")
ma_lifo.ajouter("D")
# Doit sortir dans l'ordre : D, A, C, B
print("LIFO: sorties:")
print(ma_lifo.sortir())
print(ma_lifo.sortir())
print(ma_lifo.sortir())
print(ma_lifo.sortir())
