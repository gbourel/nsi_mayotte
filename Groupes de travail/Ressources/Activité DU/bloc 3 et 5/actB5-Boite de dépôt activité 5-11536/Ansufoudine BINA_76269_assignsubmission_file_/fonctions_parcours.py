#!/bin/env python
# coding=utf-8

arbre = ["-", ["x", ["10"], ["3"]], ["+", ["5"], ["/", ["12"], ["4"]]]]
nombre = ['110',['50', ['11'], ['30', ['26']]], ['88', ['77'], ['79']] ]
excellent = ['L', ['X', ['E'], ['C', [], ['E']]], ['E', ['L'], ['T', ['N'],[]]]]

#Parcours infixe
def infixe(a):
  if a == None:
    return ""
  else:
    if len(a) == 1:
      return " " + a[0] + " "
    elif len(a) == 3:
      chaine = infixe(a[1]) + " " + a[0] + " "+ infixe(a[2])
      return chaine  
    else :
      return ""
      
      

#Parcours prefixe
def prefixe(a):
  if a == None :
    return ""
  else:
    if len(a) == 1:
       return " " + a[0] + " "
    elif len(a) == 3:
      chaine =  " " + a[0] + " " + prefixe(a[1]) + prefixe(a[2])
      return chaine
    else :
        return ""

#Parcours postfixe
def postfixe(a):
  if a == None :
    return ""
  else:
    if len(a) == 1:
       return " " + a[0] + " "
    elif len(a) == 3:
      chaine =  postfixe(a[1]) + postfixe(a[2]) + " " + a[0] + " "
      return chaine
    else :
      return ""


#Parcours en largeur
def largeur(a):
  liste = []
  chaine = ""

  if a == None :
    return chaine
  else:
    #R�cup�re le noeud
    chaine = chaine + " " + a[0]
    #Mets le sous arbre droite et gauche dans une liste
    if len(a) > 1:
      liste.append(a[1])
    if len(a) > 2:
      liste.append(a[2])
    while len(liste) > 0 :      
      #R�cup�re les noeuds des sous arbres
      if len(liste[0])>0 :
        chaine = chaine + " " + str(liste[0][0])
        #Mets les sous arbres droite et gauche dans une liste
        if len(liste[0]) > 1 :
          liste.append(liste[0][1])
        if len(liste[0]) > 2 :
          liste.append(liste[0][2])    
      #Supprime la tete de la liste    
      del(liste[0])      
    return chaine




print("Infixe : "+infixe(excellent))

print("Pr�fixe : "+prefixe(excellent))

print("Postfixe : "+postfixe(excellent))

print("Largeur : "+largeur(excellent))