var volet = ""; // Tableau de broches
var lumiere = ""; // Tableau d'etat des broches
//var adresse = "http://192.168.1.3/"; // L'url+port de votre shield

document.addEventListener('DOMContentLoaded', setup, false);

function setup() {
    // fonction qui va lier les variables a leur conteneur html
    voletF = document.getElementById("voletfermer");
	voletO = document.getElementById("voletouvert");
    lumiereE = document.getElementById("eteindre");
	lumiereA = document.getElementById("allumer");
	temp = document.getElementById("temp");
	hygro = document.getElementById("hygro");
    
    // La fonction concernant le bouton
    var boutonE = document.getElementById("envoyer");
    boutonE.addEventListener('click', executer, false);
	var boutonR = document.getElementById("recevoir");
	boutonR.addEventListener('click', recevoir, false);
}

function executer() 
	{
    // Fonction qui va créer l'url avec les paramètres puis
    // envoyer la requête
    var requete = new XMLHttpRequest(); // créer un objet de requête
    let add = document.getElementById("ip");
    if(add.value == "") {
    	alert("Veuillez entrer une adresse IP !");
    }
    else {
		adresse = "http://" + add.value;
		console.log(add.value);
		var url = adresse + "?";
		url += "V=";
		if(voletF.checked) // si la case est cochée
		{
			url += 0 + "&";
		}
		if(voletO.checked) // si la case est cochée
		{
			url += 1 + "&";
		}
		url += "L=";
		if(lumiereE.checked) // si la case est cochée
		{
			url += 0 + "&";
		}
		if(lumiereA.checked) // si la case est cochée
		{
			url += 1 + "&";
		}
		// enlève le dernier "&"
		if(url[url.length-1] === '&')
		{
		    url = url.substring(0, url.length-1);
		}
		url += "&M=0";
		console.log(url) // Pour debugguer l'url formée   
	 
		requete.open("GET", url, true); // On construit la requête
		requete.send(null); // On envoie !

		// requete.onreadystatechange = function() // on attend le retour
		// { 
		//     if (requete.readyState == 4) 
		// 	{
		//         if (requete.status == 200) // Retour s'est bien passé !
		// 		{
		//             afficher(requete.responseText); // fonction d'affichage (ci-dessous)
		//         } 
		// 		else // Retour s'est mal passé
		// 		{ 
		//             alert("ERREUR");
		//         }
		//     }
		// };
    }
}

function recevoir() 
	{
    // Fonction qui va créer l'url avec les paramètres puis
    // envoyer la requête
    var requete = new XMLHttpRequest(); // créer un objet de requête
    var url = adresse + "?";
    url += "M=1";


    console.log(url) // Pour debugguer l'url formée   
 
    requete.open("GET", url, true); // On construit la requête
    requete.send(null); // On envoie !

    requete.onreadystatechange = function() // on attend le retour
	{ 
        if (requete.readyState == 4) 
		{
            if (requete.status == 200) // Retour s'est bien passé !
			{
                afficher(requete.responseText); // fonction d'affichage (ci-dessous)
            } 
			else // Retour s'est mal passé
			{ 
                alert("ERREUR");
            }
        }
    };
}

function afficher(json) 
{
    donnees = JSON.parse(json);
    console.log(donnees);
    temp.innerHTML = parseInt(donnees["temp"]);
    hygro.innerHTML = parseInt(donnees["hygro"]);
}
