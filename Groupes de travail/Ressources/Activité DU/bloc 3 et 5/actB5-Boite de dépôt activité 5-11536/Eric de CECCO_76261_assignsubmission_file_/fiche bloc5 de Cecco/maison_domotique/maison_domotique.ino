/*

  Web Server
 
 */

#include <SPI.h>
#include <Ethernet.h>
#include <Servo.h> 

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:

byte mac[] = {0x90, 0xA2, 0xDA, 0x0F, 0xEB, 0x26};
IPAddress ip(192,168,1,3);
EthernetServer serveur(80);
int index;
int ind;
int etats[3];
char url[100];
char val[3];
int lon;
int valeur;
float capt;
int cont;

Servo myservo;

void setup() 
{
 // Open serial communications and wait for port to open:
  Serial.begin(9600);
  Serial.println("Pret");
  // start the Ethernet connection and the server:
  Ethernet.begin(mac);
  serveur.begin();
  Serial.println(Ethernet.localIP());
  myservo.attach(3);
  myservo.write(179);
}


void loop() 
{
  // Regarde si un client est connecté et attend une réponse
  EthernetClient client = serveur.available();
  if (client) 
  {
    url[0] = '\0'; // on remet à zéro notre chaîne tampon qui doit contenir la chaine reçue
    index = 0;
    while(client.connected()) 
    {
      if(client.available()) // teste si un client envoie quelque chose
      { 
        // traitement des infos du client
        char carlu = client.read(); //on lit ce qu'il envoie
        if(carlu != '\n') 
        {
          // '\n' est la fin de chaine, alors ici on stocke le caractère
          url[index] = carlu;
          index++;
        } 
        else 
        {
          // on a fini de lire ce qui nous interesse
          // on marque la fin de l'url (caractère de fin de chaîne)
          url[index] = NULL;
          Serial.print("Commande reçue : ");Serial.println(url);
          boolean ok = interpreter(); // essaie d'interpreter la chaîne (parser)
          if(ok) 
          {
            // tout s'est bien passé = on met à jour les broches
            Serial.println("Voici les ordres interpretes :");
            Serial.print("Volet :");Serial.println(etats[0]);
            Serial.print("Lumiere :");Serial.println(etats[1]);
            Serial.print("Mesure :");Serial.println(etats[2]);
            action();
            delay(500);
            if(etats[2] == 1)
            {
              envoyer_mesures(client);
            }         
          }
          break;
        }
      }
    }
    // Donne le temps au client de prendre les données
    delay(10);
    // Ferme la connexion avec le client
    client.stop();
    Serial.println("Fin ! Client deconnecte");
  }
}

boolean interpreter() 
{
   // on interprete la chaine reçue : GET ..... /?V=val1&L=val2&M=val3 HTTP/1.1
   
  etats[0] = 0;
  etats[1] = 0;
  etats[2] = 0;
  index = 4;
  
  // On cherche le marqueur "?" (debut des parametres)
  Serial.println("On parse la chaine...");
  cont = 1;
  while(url[index] != '?') 
  { 
    index++;
    if(index == 100) 
    {
      // On est rendu trop loin, donc il y a un probleme sur la chaine
      Serial.println("La chaine n'est pas conforme, impossible à perser.");
      return false;
    }
  }
  while(cont)
  {
  // si on trouve "V="
    if(url[index-1] == 'V' && url[index] == '=')
    {
      ind = index;
      lon = 0;
      for(int i = 0; i < 3; i++)
      {
        val[i] = '0';
      }
      // on va jusqu'au caractere '&' ou '\n'
      while((url[index+1] != '&') && (url[index+1] != ' '))
      {
         if(url[index+1] == ' ') { cont = 0;}
         val[index-ind] = url[index+1];
         index++;
         lon++;
      }
      for(int i = 0; i < lon; i++) 
      { 
        etats[0] = (val[i]-'0') * int(pow(10,lon-i-1)) + etats[0];// on transforme les caractères en nombre
      }
    }
    // si on trouve "L="
    if(url[index-1] == 'L' && url[index] == '=') 
    {
      ind = index;
      lon = 0;
      for(int i = 0; i < 3; i++)
      {
        val[i] = '0';
      }
      // on va jusqu'au caractere '&' ou '\n'
      while((url[index+1] != '&') && (url[index+1] != ' '))
      {
         if(url[index+1] == ' ') { cont = 0;}
         val[index-ind] = url[index+1];
         index++;
         lon++;
      }
      for(int i = 0; i < lon; i++) 
      { 
        etats[1] = (val[i]-'0') * int(pow(10,lon-i-1)) + etats[1];// on transforme les caractères en nombre
      }
    }
    // si on trouve "M="
    if(url[index-1] == 'M' && url[index] == '=') 
    {
      ind = index;
      lon = 0;
      for(int i = 0; i < 3; i++)
      {
        val[i] = '0';
      }
      // on va jusqu'au caractere '&' ou '\n'
      while((url[index+1] != '&') && (url[index+1] != ' '))
      {
         if(url[index+1] == ' ') { cont = 0;}
         val[index-ind] = url[index+1];
         index++;
         lon++;
      }
      for(int i = 0; i < lon; i++) 
      { 
        etats[2] = (val[i]-'0') * int(pow(10,lon-i-1)) + etats[2];// on transforme les caractères en nombre
      }
    }

    index++;
    if(index >= 100)
    {
      cont = 0;
    }
  }
 
  if(index > 100)
  {
    Serial.println("Probleme de parser");
    return false;
  }
  return true;
}

void action() 
{
  // On met à jour nos broches
  digitalWrite(6, etats[0]);
  digitalWrite(5, etats[1]);
}



void envoyer_mesures(EthernetClient client) 
{
  // On fait notre en-tête
  // Tout d'abord le code de réponse 200 = réussite
  client.println("HTTP/1.1 200 OK");
  // Puis le type mime du contenu renvoyé, du json
  client.println("Content-Type: application/json");//
  // Autorise le cross origin
  client.println("Access-Control-Allow-Origin: *");
  // Et c'est tout !
  // On envoi une ligne vide pour signaler la fin du header
  client.println();

  // Puis on commence notre JSON par une accolade ouvrante
  client.print("{");
  // On envoie la première clé 
  client.print("\"temp\":");
  // Puis la valeur
    valeur = analogRead(A0);
    capt=(float)(1023-valeur)*10000/valeur; 
    capt=1/(log(capt/10000)/3975+1/298.15)-273.15;
    client.print(capt);
  //Une petite virgule pour séparer les deux clés
  client.print(",");
  // Et on envoie la seconde 
  client.print("\"hygro\":");
    valeur = analogRead(A1);
    capt = (float)valeur/1023*100; //que du bloeuf !
  client.print(capt);
  client.print("}");
  client.println();

}
