#!/usr/bin/env python
""" Création de la table pour les continents dans la bdd monde.db """

import sqlite3

try:
    sqliteConnexion = sqlite3.connect('Monde.db')
    requete_creation_table = '''CREATE TABLE Continents (
                                id INTEGER PRIMARY KEY AUTOINCREMENT,
                                code TEXT NOT NULL,
                                nom TEXT NOT NULL,
                                carte BLOB,
                                superficie REAL NOT NULL);'''
    curseur = sqliteConnexion.cursor()
    print("Succès ! Connecté au monde ! ")
    curseur.execute(requete_creation_table)
    sqliteConnexion.commit()
    print("Table SQL des Continents créée ! ")
    curseur.close()

except sqlite3.Error as erreur:
    print("Erreur de création de table SQLite ", erreur)
finally:
    if sqliteConnexion:
        sqliteConnexion.close()
        print("Connexion sqlite fermée")
