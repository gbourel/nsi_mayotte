#!/usr/bin/env python
""" Exemple simple d'utilisation d'une base de données """

import sqlite3

try:
    connexionSqlite = sqlite3.connect('monde.db')
    curseur = connexionSqlite.cursor()
    print("Base de données créée et connectée à SQLite")

    requeteSelect = "select sqlite_version();"
    curseur.execute(requeteSelect)
    donnees = curseur.fetchall()
    print("Version de la base de données SQLite : ", donnees)

except sqlite3.Error as erreur:
    print("Erreur lors de la connexion à sqlite : ", erreur)
finally:
    if connexionSqlite:
        connexionSqlite.close()
        print("La connexion SQLite est refermée.")
