#!/usr/bin/env python
""" RENDU des CARTEs de la table des CONTINENTs dans une fenêtre """
import sqlite3
import io
import tkinter as Tk
from PIL import Image, ImageTk

# on prépare un objet 'fenetre' graphique...
fenetre = Tk.Tk()
fenetre.title("7 Continents")
fenetre.iconbitmap("globe.ico")

# Voici le premier CONTINENT à afficher au démarrage.
CONTINENT = 6
# On crée l'objet NOM_LBL avec rien dedans pour pouvoir le détruire...
# Essayez de commenter la ligne suivante ET la ligne NOM_LBL.destroy()
NOM_LBL = Tk.Label(fenetre, text="", font=("", 16))


def va_chercher(CONTINENT):
    """ va chercher le CONTINENT et peuple les objets pour la fenêtre """
    # Déclaration des variables globales nécessaires.... pour faire quoi ?
    global RENDU
    global NOM_LBL

    # On évite d'aller chercher le huitième CONTINENT qui n'existe pas
    # c'est le profilage de la variable
    if CONTINENT < 1:
        CONTINENT = 1
    if CONTINENT > 7:
        CONTINENT = 7

    # On prépare une chaine de caractères avec la requete SQL
    # avec des ? pour les paramètres
    requete_sql = "SELECT * from CONTINENTs where id = ?"
    # On positionne le curseur sur le CONTINENT désiré
    curseur.execute(requete_sql, str(CONTINENT))
    # On récupère les données pointées par le curseur
    donnees = curseur.fetchall()
    # On imprime sur la console les données structurées récupérées
    for COLONNE in donnees:
        print("Id = ", COLONNE[0],
                "Code = ", COLONNE[1],
                "Nom = ", COLONNE[2],
                "Superficie = ", COLONNE[4])
        CARTE = COLONNE[3]    # la CARTE -> 3eme COLONNE de la table
    # On prépare la CARTE pour pouvoir l'afficher (c'est un BLOB)
    objet_fichier = io.BytesIO(CARTE)  # donc un flux binaire
    DESSIN = Image.open(objet_fichier)  # on le donne à Tkinter
    DESSIN = DESSIN.resize((600, 600), Image.ANTIALIAS)  # mise à l'échelle
    # Dessine l'image dans un canevas (fenêtre qui se met devant)
    RENDU = ImageTk.PhotoImage(DESSIN)
    canevas = Tk.Canvas(fenetre, width=DESSIN.size[0], height=DESSIN.size[1])
    canevas.create_image(0, 0, anchor=Tk.NW, image=RENDU)
    canevas.grid(row=2, column=0, padx=0, pady=0)
    # redéfinit les étiquettes pour écrire le nom du CONTINENT
    NOM_LBL.destroy()  # pour effacer l'ancien titre (à essayer de commenter)
    NOM_LBL = Tk.Label(fenetre, text=COLONNE[2], font=("", 16))
    NOM_LBL.grid(row=0, columnspan=2, pady=8)
    info_lbl = Tk.Label(fenetre, text="Code : "+str(COLONNE[0])
                        + " Superficie = "+str(COLONNE[4]), font=("", 10))
    info_lbl.grid(row=1, column=0, padx=15, pady=5)


def roulette(event):
    """ Action quand la roulette a tourné """
    # on pourra essayer de commenter la déclaration globale suivante
    # mais il faudra expliquer ce qui se passe... ou ne se passe plus !
    global CONTINENT

    # Pour incrémenter/décrémenter le numéro du CONTINENT
    if event.delta < 0:
        CONTINENT -= 1
    elif event.delta > 0:
        CONTINENT += 1
    va_chercher(CONTINENT)




try:
    connexionSqlite = sqlite3.connect('monde.db')
    curseur = connexionSqlite.cursor()
    print("Base de données monde.db connectée à SQLite")

    va_chercher(CONTINENT)

    # autres objets à créer : roulette, bouton quitter...
    fenetre.bind('<MouseWheel>', roulette)

    # définit un bouton Quitter
    bouton = Tk.Button(fenetre, text='Quitter', command=fenetre.quit)
    bouton.grid(row=3, column=0, padx=15, pady=5)

    # fait vivre la fenêtre et ses objets
    fenetre.mainloop()  # on boucle sur l'objet graphique fenetre.


except sqlite3.Error as erreur:
    print("Erreur lors de la connexion à sqlite : ", erreur)

finally:
    if connexionSqlite:
        connexionSqlite.close()
        print("La connexion SQLite est refermée.")
