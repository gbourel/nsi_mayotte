#!/usr/bin/env python
""" Remplissage de la table des continents dans la bdd monde.db """

import sqlite3


def insertBLOB(code, nom, superficie, carte):
    """ Voici la fonction qui connecte, insère et referme à chaque appel """
    try:
        connexionSqlite = sqlite3.connect('monde.db')
        # passer l'application en chaines Unicode
        connexionSqlite.text_factory = str
        curseur = connexionSqlite.cursor()
        print("Connexion au monde réussie avec SQLite")
        sql = "INSERT INTO continents (code, nom, carte, superficie) VALUES (?, ?, ?, ?)"

        # Convertir le fichier au format binaire
        with open(carte, 'rb') as monfichier:
            dessinCarte = monfichier.read()
        valeur = (code, nom, dessinCarte, superficie)
        curseur.execute(sql, valeur)
        connexionSqlite.commit()
        print("Données inserées avec succès pour l'", nom)
        curseur.close()
        connexionSqlite.close()
        print("Connexion SQLite fermée.")

    except sqlite3.Error as erreur:
        print("Erreur lors de l'insertion ", erreur)

""" Programme principal. Insère 6 continents différents avec des données spécifiques """
# insertBLOB("XX", "Continent", 12, "C:\Users\jfp\Desktop\\image.png")
insertBLOB("AS", "Asie", 44579000, "Asia.png")
insertBLOB("AF", "Afrique", 30065000, "Africa.png")
insertBLOB("NA", "Amérique du Nord", 24256000, "North_America.png")
insertBLOB("SA", "Amérique du Sud", 17819000, "South_America.png")
insertBLOB("AN", "Antarctique", 13209000, "Antarctica.png")
insertBLOB("EU", "Europe", 9938000, "Europe.png")
insertBLOB("OC", "Océanie", 7687000, "Oceania.png")
