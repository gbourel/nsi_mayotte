import time

def calculAngleRotation(differenceCapteur) :
	angle = 0
	if ( abs(differenceCapteur) ) >= 10.0) :
		angle = 10
	elif ( abs(differenceCapteur) ) >= 5.0) :
		angle = 8
	elif ( abs(differenceCapteur) >= 1.0) :
		angle = 3
	elif ( abs(differenceCapteur) >= 0.5 ) :
		angle = 1

	return angle


while(soleil == True) :

	# Calcul de l'angle de rotation horizontal
	differenceHorizontal = (capteurHautGauche + capteurBasGauche)/2 - (capteurHautDroit + capteurBasDroit)/2
	angleRotationHorizontal = calculAngleRotation(differenceHorizontal)

	# Calcul de l'angle de rotation vertical
	differenceVertical = (capteurHautGauche + capteurHautDroit)/2 - (capteurBasGauche + capteurBasDroit)/2
	angleRotationVertical = calculAngleRotation(differenceVertical)

	# sens de rotation horizontal
	if(differenceHorizontal > 0) :
		tournerVersGauche(angle)
	elif(differenceHorizontal < 0)
		tournerVersDroite(angle)

	# sens de rotation vertical
	if(differenceVertical > 0) :
		tournerVersHaut(angle)
	elif(differenceVertical < 0)
		tournerVersBas(angle)

	# attendre 1 minute avant la prochaine mesure
	time.sleep(60) # Sleep for 60 seconds

def chercherSoleilDansEspace() :
	angleIdealHorizontal = 0
	angleIdealVertical = 0
	puissanceSolaire = 0
	for angleVertical in range (180) :
		tournerVersHaut(1)
		for angleHorizontal in range (360) :
			tournerVersGauche(1)
			if capteurHautGauche > puissanceSolaire :
				puissanceSolaire = capteurHautGauche
				angleIdealVertical = angleVertical
				angleIdealHorizontal = angleHorizontal
	return angleIdealHorizontal, angleIdealVertical