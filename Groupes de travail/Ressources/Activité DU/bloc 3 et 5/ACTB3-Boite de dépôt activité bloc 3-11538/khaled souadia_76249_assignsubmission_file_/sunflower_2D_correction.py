import time

# Initialisation de la position
tournerVersGauche(chercherSoleil())

# Correction de la position
while(soleil == True) :

	# Calcul de l'angle de rotation
	difference = capteurGauche - capteurDroit
	if ( abs(difference) ) >= 10.0) :
		angle = 10
	elif ( abs(difference) ) >= 5.0) :
		angle = 8
	elif ( abs(difference) >= 1.0) :
		angle = 3
	elif ( abs(difference) >= 0.5 ) :
		angle = 1

	# sens de rotation
	if(capteurGauche > capteurDroit) :
		tournerVersGauche(angle)
	elif(capteurDroit < capteurGauche)
		tournerVersDroite(angle)

	# attendre 1 minute avant la prochaine mesure
	time.sleep(60) # Sleep for 60 seconds

def chercherSoleil() :
	angleIdeal = 0
	puissanceSolaire = 0
	for i in range (360) :
		tournerVersGauche(1)
		if capteurGauche > puissanceSolaire :
			puissanceSolaire = capteurGauche
			angleIdeal = i
	return angleIdeal