
//Feux 1
const int feu_rouge_1 = 11;
const int feu_orange_1 = 12;
const int feu_vert_1 = 13;

//Feux 2
const int feu_rouge_2 = 4;
const int feu_orange_2 = 3;
const int feu_vert_2 = 2;

const int temps_cycle = 15000; //Le temps de faire un cycle complet sur un feu
int etat_feu_1 = 1; // 1 = vert et 0 = rouge
int temps = 0;

const int delai_orange = 2000;

//Met la led a l_etat indique
void action_led(int nom, bool etat) {
  if(etat){
    digitalWrite(nom, HIGH);
  }else {
 	 digitalWrite(nom, LOW);
  }
}


void setup(){
  pinMode(feu_rouge_1, OUTPUT);
  pinMode(feu_orange_1, OUTPUT);
  pinMode(feu_vert_1, OUTPUT);
  
  pinMode(feu_rouge_2, OUTPUT);
  pinMode(feu_orange_2, OUTPUT);
  pinMode(feu_vert_2, OUTPUT);
}

void loop(){
  
  if(etat_feu_1 == 1) {
    //Feu 1
    action_led(feu_vert_1, 1); //Allumer feu vert 1
    action_led(feu_orange_1, 0); //Eteindre feu orange 1
    action_led(feu_rouge_1, 0); //Eteindre feu rouge 1
    
    //Feu 2
    action_led(feu_vert_2, 0); //Eteindre feu vert 2
    action_led(feu_orange_2, 0); //Eteindre feu orange 2
    action_led(feu_rouge_2, 1); //Allumer feu rouge 2
    
  }
  else{
    //Feu 1
    action_led(feu_vert_1, 0); //Eteindre feu vert 1
    action_led(feu_orange_1, 0); //Eteindre feu orange 1
    action_led(feu_rouge_1, 1); //Allumer rouge 1
    
    //Feu 2
    action_led(feu_vert_2, 1); //Allumer feu vert 2
    action_led(feu_orange_2, 0); //Eteindre feu orange 2
    action_led(feu_rouge_2, 0); //Eteindre feu rouge 2
  }
  
  //Incremente le temps et remise a 0 si depasse la limite maxi
  temps += 1000;
  if(temps > temps_cycle){
    temps = 0;
  }
  
  //Changement detat et attente
  //Passage du feu vert 1 au feu rouge
  if(etat_feu_1 == 1 && temps > temps_cycle / 2){	
    etat_feu_1 = 0;
    action_led(feu_vert_1, 0); //Eteindre feu vert 1
    action_led(feu_orange_1, 1); //Allumer feu orange 1
    delay(delai_orange);	
  }
  //Passage du feu vert 2 au feu rouge
  else if(etat_feu_1 == 0 && temps <= temps_cycle / 2){
    etat_feu_1 = 1;
    action_led(feu_vert_2, 0);//Eteindre feu vert 2
    action_led(feu_orange_2, 1);//Allumer feu orange 2
    delay(delai_orange);
  }
  
  delay(1000);
  
}