/*
  Feux de travaux, 
  attente de 5 secs pour laisser les vehicules engages le temps de liberer
  la voix avant de laisser le tour a l_autre sens
*/

const int feu_rouge_1 = 12;
const int feu_orange_1 = 13;

const int feu_rouge_2 = 2;
const int feu_orange_2 = 3;

int temps_cycle = 30000;
int temps = 0;

int etat_feu_orange_1 = 0;
int etat_feu_orange_2 = 0;

int etat_feu_1 = 1;
  
  
//Met la led a l_etat indique
void action_led(int nom, bool etat) {
  if(etat){
    digitalWrite(nom, HIGH);
  }
  else {
 	 digitalWrite(nom, LOW);
  }
}

void liberer_voie(){
  //Feux 1
  action_led(feu_rouge_1, 1);
  action_led(feu_orange_1, 0);
  etat_feu_orange_1 = 1;
  //Feux 2
  action_led(feu_rouge_2, 1);
  action_led(feu_orange_2, 0);
  etat_feu_orange_2 = 1;
  
  delay(5000);
}

void setup(){
  //Feux 1
  pinMode(feu_rouge_1, OUTPUT);
  pinMode(feu_orange_1, OUTPUT);
  //Feux 2
  pinMode(feu_rouge_2, OUTPUT);
  pinMode(feu_orange_2, OUTPUT);
  
  Serial.begin(9600);
  
}

void loop(){   
  
  if (etat_feu_1 == 1) {      
	//Feux 1 clignote
	if(etat_feu_orange_1) {
		action_led(feu_orange_1, 1);
		etat_feu_orange_1 = 0;
	}
    else {
		action_led(feu_orange_1, 0);
		etat_feu_orange_1 = 1;
	}
    
	action_led(feu_rouge_1, 0);
	//Feu 2 rouge fixe
	action_led(feu_rouge_2, 1);
    action_led(feu_orange_2, 0);
    etat_feu_orange_2 = 0;        
  }
  else {
    //Feux 2 clignote
    if(etat_feu_orange_2) {
		action_led(feu_orange_2, 1);
		etat_feu_orange_2 = 0;
	}
	else {
		action_led(feu_orange_2, 0);
		etat_feu_orange_2 = 1;
	}  
    
	action_led(feu_rouge_2, 0);
	//Feu 1 rouge fix
  	action_led(feu_rouge_1, 1);
    action_led(feu_orange_1, 0);
    etat_feu_orange_1 = 1;
  }
  //Incremente le temps et remise a 0 si depasse la limite maxi
  temps += 1000;
  if(temps > temps_cycle){
    temps = 0;
  }
  
  //Changement detat et attente
  if(etat_feu_1 == 1 && temps > temps_cycle / 2){
    etat_feu_1 = 0;
	liberer_voie();	
  }
  else if(etat_feu_1 == 0 && temps <= temps_cycle / 2){
    etat_feu_1 = 1;
    liberer_voie();	
  }
  
  delay(1000);
}